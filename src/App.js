import React, { Component } from 'react';
import {geolocated} from 'react-geolocated';
import './App.css';
import Weather from './components/Weather';

class App extends Component {
  render() {
    return !this.props.isGeolocationAvailable ? <div>
      Your browser does not support Geolocation
     </div> : !this.props.isGeolocationEnabled ? <div>
      Geolocation is not enabled
     </div> : this.props.coords ? <div className="App">
      <Weather current_lat={this.props.coords.latitude} current_lng={this.props.coords.longitude}  />
     </div> : <div />;
   }
}

export default geolocated({
  positionOptions: {
    enableHighAccuracy: false,
  },
  userDecisionTimeout: 5000,
})(App);
