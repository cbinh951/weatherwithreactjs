import React, { Component } from 'react';

class WeatherDetail extends Component {
    convertTime(time){
      return new Date(1000 * time).toLocaleTimeString();
    }

    convertWindToDirection(degree) {
      if(typeof degree !== 'undefined'){
          var sectors = ['Northerly','North Easterly','Easterly','South Easterly','Southerly','South Westerly','Westerly','North Westerly'];
          degree += 22.5;
          degree = degree < 0 ? 360 - Math.abs(degree) % 360 : degree % 360;
          return sectors[parseInt(degree / 45, 0)];
      }
      return 'Northerly';
    }

    convertCelsiusToFahrenheit(degree){
      return Math.round(parseFloat(degree * 1.8 + 32) * 1000) / 1000;
    }

    determine(code){
      return ((code >= 300 && code <= 321) || (code >= 500 && code <= 531)) ? "Person needs an umbrella" : "Person needs a coat";
    }

    statusDay(code){
      if(code <= 232) return "Storm today, please be careful";
      if(code <= 531) return "Today the sky has rain. You should bring an umbrella when you go out";
      if(code <= 622) return "Today the sky has snow. You should keep your body";
      if((code >= 800 && code <= 804) || (code >= 951 && code <= 955)) return "It's a nice day";
      return "Today's bad weather. You should stay home";
    }

    render() {
      const data = this.props.infoWeather;
      if(data){
        return (
          <div>
            <h1><strong>{this.statusDay(data.weather[0].id)}</strong></h1>
            <h2>{data.name}</h2>
            <h2>Weather: <img alt="icon" src={'http://openweathermap.org/img/w/'+ data.weather[0].icon  +'.png'} />{data.weather[0].main}</h2>
            <h2>Description: {data.weather[0].description} </h2>
            <h2>Sunrise: {this.convertTime(data.sys.sunrise)}</h2>
            <h2>Sunset:  {this.convertTime(data.sys.sunset)}</h2>
            <h2>Humidity:{data.main.humidity}%</h2>
            <h2>Wind: {this.convertWindToDirection(data.main.deg)}</h2>
            <h2>Temperature: {data.main.temp} Celsius/ {this.convertCelsiusToFahrenheit(data.main.temp)} Fahrenheit</h2>
            <h2><strong>Determine: {this.determine(data.weather[0].id)}</strong></h2>  
          </div>
        );
      }else{
        return (
          <div>
            <h2>City not found</h2>
          </div>
        );
      }
    }
}

export default WeatherDetail;
