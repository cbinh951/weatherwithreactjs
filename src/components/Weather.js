import React, { Component } from 'react';
import PlacesAutocomplete, { geocodeByAddress } from 'react-places-autocomplete';
import '../App.css';
import WeatherDetail from './WeatherDetail';

const keyOpenWeatherMap = '38cf54881f69412359a802e3bd7a8ed5';
class Weather extends Component {
    constructor(props) {
        super(props);
        this.state = { 
                        address: '', 
                        checkData: false, 
                        infoWeather: '' 
                    };
        this.onChange = (address) => this.setState({ address });
        this.getInfoWeather = this.getInfoWeather.bind(this);
    }

    componentWillMount(){
        this.getInfoWeather(this.props.current_lat, this.props.current_lng);
    }
    
    handleFormSubmit = (event) => {
        event.preventDefault()

        geocodeByAddress(this.state.address, (err, latLng) => {
            if(!err){
                this.getInfoWeather(latLng.lat, latLng.lng);
            }else{
                this.getInfoWeather(this.props.current_lat, this.props.current_lng);
            }
        })
    }

    getInfoWeather(lat, lon) {
        fetch(`http://api.openweathermap.org/data/2.5/weather?lat=${lat}&lon=${lon}&units=metric&APPID=${keyOpenWeatherMap}`)
        .then(results => {
            return results.json();
        }).then(data => {
            this.setState({
                infoWeather: data,
                checkData: true
            });
        }).catch(function () {
            console.log('City not found');
        });
    }

    render() {
        const inputProps = {
            value: this.state.address,
            onChange: this.onChange,
        }
        
        return (
            <div>
                <form onSubmit={this.handleFormSubmit} className="input">
                    <PlacesAutocomplete inputProps={inputProps} />
                    <button className="btnSearch btn btn-primary">Search City</button>
                </form>
                <WeatherDetail infoWeather={this.state.checkData ? this.state.infoWeather : null} />
            </div>
        );
    }
}

export default Weather;
